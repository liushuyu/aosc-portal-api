use toml;
use serde_derive::Deserialize;

#[derive(Deserialize, Debug)]
struct Mirror {
    name: String,
    url: String,
    region: String,
}

#[derive(Deserialize, Debug)]
pub struct MirrorConfig {
    main: Mirror,
    mirrors: Vec<Mirror>,
}
