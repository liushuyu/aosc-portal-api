use reqwest::r#async::Client;
use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT};
use std::thread;
use std::sync::{Arc, Mutex};
use std::sync::mpsc;
use std::sync::mpsc::channel;
use crate::config::MirrorConfig;

const API_USER_AGENT: &'static str = "AOSCMirrorWatcher/0.1";

struct MirrorWatcherState {
    pub config: Option<MirrorConfig>,
}

struct MirrorWatcher {
    client: Client,
    watcher_tx: mpsc::Sender<MirrorWatcherState>,
}

impl MirrorWatcher {
    pub fn new(config: MirrorConfig) -> MirrorWatcher {
        let mut headers = HeaderMap::new();
        headers.insert(USER_AGENT, HeaderValue::from_static(API_USER_AGENT));
        let (tx, rx) = channel();

        thread::spawn(move || {
            MirrorWatcher::worker(config, rx);
        });

        MirrorWatcher {
            client: Client::builder().cookie_store(true).build().unwrap(),
            watcher_tx: tx,
        }
    }

    pub fn reload(&mut self, config: MirrorConfig) {
        let state = MirrorWatcherState {
            config: Some(config),
        };

    }

    pub fn stop(&mut self) {
        let state = MirrorWatcherState {
            config: None,
        };
    }

    fn worker(initial_config: MirrorConfig, rx: mpsc::Receiver<MirrorWatcherState>) {

    }

}
